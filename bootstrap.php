<?php

include_once('vendor/autoload.php');

use app\Application;
use app\core\di\Container;


$container = new Container();

try{
    $container->register(new \app\diBox\RequestBox());
}catch (\app\core\exception\DIException $e){
    echo '<pre>';
    var_dump($e);
}

return new Application($container);
