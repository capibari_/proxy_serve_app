<?php


namespace app\controller;


class DefaultController
{
    protected $request;
    protected $data;

    public function __construct($request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    protected function debug($data)
    {
        echo '<pre>';
        var_dump($data);
        die;
    }
}