<?php


namespace app\controller;


class ProxyController extends DefaultController
{

    public function actionSetConfig()
    {
        $filename = $this->data->path.DIRECTORY_SEPARATOR.$this->data->file;
        return file_put_contents ($filename, $this->data->config);
    }

    public function actionRestartService()
    {
        switch ($this->data->os) {
            case 'win':
                exec('restart.bat 2>&1', $output);
                break;
            case 'ubuntu':
                exec('sudo service 3proxy restart 2>&1', $output);
                exec('sudo journalctl -u 3proxy', $output);
                $output = $output[sizeof($output)-1];
                break;
        }

        return serialize($output);
    }

}