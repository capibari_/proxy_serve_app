<?php


namespace app\controller;


class CronController extends DefaultController
{
    public function actionSetCron()
    {
        switch ($this->data->os) {
            case 'win':
                break;
            case 'ubuntu':
                $cron_file = sprintf('%s%stemp%s%s',
                    realpath(__DIR__ . '/../..'),DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, 'cron.temp');
                if(file_put_contents($cron_file, $this->data->cron)){
                    exec("sudo crontab -r  2>&1", $output);
                    exec("sudo crontab ".$cron_file." 2>&1", $output);
                    exec("sudo service cron restart 2>&1", $output);
                }
                break;
        }

        return serialize($output);
    }

    public function actionRestartCron()
    {
        switch ($this->data->os) {
            case 'win':
                break;
            case 'ubuntu':
                exec ("sudo service cron restart", $output);
                break;
        }

        return serialize($output);
    }

}