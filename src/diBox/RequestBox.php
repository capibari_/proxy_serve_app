<?php


namespace app\diBox;


use app\core\db\DBConnector;
use app\core\db\DBDriver;
use app\core\di\Container;
use app\core\di\ContainerInterface;
use app\core\tools\Request;

class RequestBox implements ContainerInterface
{
    public function register(Container $container)
    {
        $container->service('request', function(){
            return new Request($_GET, $_POST, $_SERVER, $_COOKIE, $_SESSION, $_FILES);
        });
    }
}