<?php
namespace app;

use app\components\Router;
use app\core\di\Container;
use app\core\exception\DIException;
use app\core\tools\UrlManager;

use app\Models\Data;
use Throwable;


class Application
{
    private $container;
    private $request;

    private $uri;
    private $data;

    private $controller;
    private $action;

    public function __construct(Container $container = null)
    {
        $this->container = $container ?? new Container();
        $this->request = $this->requestInit();
        $this->data = $this->DataInit();

    }

    public function run()
    {
        $this->parseUrl();
        $this->execute();
    }

    private function parseUrl()
    {
        $this->controller = UrlManager::getController();
        $this->action = UrlManager::getAction();
        $this->uri = UrlManager::getUri();
    }

    private function execute()
    {
        try{
            if(file_exists(sprintf('%s%s%s.php',
                    __DIR__,DIRECTORY_SEPARATOR, explode('app'.DIRECTORY_SEPARATOR, $this->uri)[1]))
            ){
                $controller = new $this->controller($this->request, $this->data);
                $action = (string)$this->action;
                if(method_exists($controller, $this->action)){
                    print_r($controller->$action());
                } else {
                    echo('no action');
                    return false;
                }
            } else {
                echo('no controller');
                return false;
            }
        } catch (\Throwable $e) {
            echo('<pre>');
            echo(get_class($this));
            var_dump($e);
            var_dump($e->getCode());
            var_dump($e->getTrace());
        }
    }

    private function DataInit()
    {
        return new Data($this->request->post());
    }

    private function requestInit()
    {
        try{
            return $this->container->get('request');
        }catch (DIException $e){
            echo '<pre>';
            var_dump($e->getErrors());
        }

    }
}