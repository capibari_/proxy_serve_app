<?php

namespace app\Models;

class Data
{
    public $os;
    public $path;
    public $file;
    public $link;
    public $config;
    public $cron;
    public $modem;

    public function __construct($request)
    {
        $this->os = $request['os'];
        $this->path = $request['path'];
        $this->file = $request['file'];
        $this->link = $request['path'].DIRECTORY_SEPARATOR.$request['file'];
        $this->config = $request['config'];
        $this->cron = $request['cron'];
        $this->modem = $request['modem'];
    }
}