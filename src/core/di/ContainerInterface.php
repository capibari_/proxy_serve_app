<?php


namespace app\core\di;


interface ContainerInterface
{
    public function register(Container $container);
}