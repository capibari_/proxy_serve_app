<?php


namespace app\core\di;

use app\core\exception\DIException;
use app\core\tools\Dump;

class Container
{
    public $factory = [];
    public $service = [];

    public function factory($name, \Closure $callback)
    {
        $this->factory[$name] = $callback;
    }

    public function service($name, \Closure $callback, $params = [])
    {
        $this->service[$name] = $this->invoke($callback, $params);
    }

    public function register(ContainerInterface $box)
    {
        $box->register($this);
    }

    public function get($name)
    {
        if(!isset($this->service[$name])) {
            throw new DiException(sprintf('<b>DI error:</b> Service %s does not found.', $name));
        }

        return $this->service[$name];
    }

    public function getFactory($name = '')
    {
        if(!isset($this->factory[$name])) {
            throw new DiException(sprintf('<b>DI error:</b> Factory %s does not found.', $name));
        }

        return $this->factory[$name];
    }

    public function fabricate(string $id, ...$params)
    {
        if (!isset($this->factory[$id])) {
            throw new DIException(sprintf('<b>DI error:</b> Factory %s does not found.', $id));
        }

        return $this->invoke($this->factory[$id], $params);
    }

    public function invoke(\Closure $closure, $params = [])
    {
        if (!method_exists($closure, '__invoke')) {
            throw new DiException('<b>DI error:</b> Passed is not a Closure or invokable object.');
        }
//        Dump::out($closure, $params);
        return call_user_func_array($closure, $params);
    }
}