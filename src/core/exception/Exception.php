<?php
/**
 * Created by PhpStorm.
 * UserForm: capibari
 * Date: 14.08.2018
 * Time: 0:07
 */

namespace app\core\exception;


use app\core\Tools\Dump;

class Exception extends \Exception
{
    private $errors;

    public function __construct($errors)
    {
        $this->errors = $errors;
        parent::__construct();
    }

    public function getErrors()
    {
        $errors = $this->errors;

        while(count($errors))
        {
            $key = key($errors);
            if(is_array($arr = array_shift($errors))) break;
            $array[$key][] = $arr;
            $this->errors = $array;
        }

        return $this->errors;

    }
}


