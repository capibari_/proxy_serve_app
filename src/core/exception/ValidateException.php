<?php

namespace app\core\exception;

class ValidateException extends Exception
{
    public function __construct($errors)
    {
        parent::__construct($errors);
    }
}