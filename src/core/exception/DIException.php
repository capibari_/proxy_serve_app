<?php
/**
 * Created by PhpStorm.
 * UserForm: capibari
 * Date: 14.08.2018
 * Time: 0:07
 */

namespace app\core\exception;


use app\core\Tools\Dump;

class DIException extends \Exception
{
    private $errors;

    public function __construct($error)
    {
        parent::__construct();
        $this->errors = $error;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}

