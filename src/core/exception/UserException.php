<?php


namespace app\core\exception;


class UserException extends Exception
{
    public function __construct($errors)
    {
        parent::__construct($errors);
    }
}
